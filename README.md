PML-HY
=================================

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/3ee61e8cfff04d95b7f1ac682286560b)](https://www.codacy.com/app/lippertsjan/pml-hy?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=ironjan/pml-hy&amp;utm_campaign=Badge_Grade)

This is a webapp for my participation in the course [Project in Practical Machine Learning](https://www.cs.helsinki.fi/en/courses/582739/2017/k/k/1)
at [Helsinkin Yliopistio (University of Helsinki)](https://www.helsinki.fi/fi). The application itself is online at
[[https://pppb.herokuapp.com/]].

*Description of this course:* The purpose of the course is to introduce students to the problematics of machine learning
in a realistic setting. Students should be able to identify and take into account the "dirtiness" of real online data;
select, justify and implement a machine learning algorithm/technique using a programming environment runnable on a web
server; monitor and report the accuracy of their implementation, including reflection of their choices.

# Project Structure

 * Play, Slick.
