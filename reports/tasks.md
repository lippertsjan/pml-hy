# Third Report: Technology

Please submit a two-page PDF report, explaining your run-time technology choices.

In your report consider at least the following topics:

 * Give the URL of your (hopefully functional) system
 * Describe and motivate the technology choices you have made
 * Describe any problems you might have encountered when periodically importing data
 * Describe how your system is performing

# Final Report (Prelim)

Please submit your Final report. This submission is preliminary and you will get feedback regarding your grade target. Please report your feelings and grade target in the text box and use the file upload for the actual report.

Your report should consist of the following parts:

 * Topic introduction
  * What question does your work answer
  * What related work does exist
  * Why did you choose your topic
 * Methodology choice
  * Introduction of chosen methodology
  * Motivation behind choice
  * Discussion of possible problems with data or methodology
Comparison between methodologies, if applicable
 * Run-time technology
  * Introduction of run-time system
  * Technology choices and motivation
Discussion of possible problems with setting up run-time system
 * Results
  * Result of your work (hypothesis statement)
  * Discussion of possible future work
Reflection against existing literature
 * Reflection
  * What did you learn